/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import conexion.Conexion;
import dao.ClienteDao;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cliente;

@ManagedBean
@SessionScoped
public class ClienteController implements Serializable{
    
    Conexion conn = new Conexion();
    ClienteDao clienteDao;
    Cliente cliente;
    List<Cliente> lista;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getLista() {
        return lista;
    }

    public void setLista(List<Cliente> lista) {
        this.lista = lista;
    }
    
    
    
    @PostConstruct
    public void init(){
    clienteDao = new ClienteDao(conn);
    cliente = new Cliente();
    consultar();
    }
    
    public void consultar(){
        try {
            lista=clienteDao.consultar();
        } catch (Exception e) {
        }
    }
}
