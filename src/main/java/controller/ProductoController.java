
package controller;

import conexion.Conexion;
import dao.ClienteDao;
import dao.ProductoDao;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cliente;
import modelo.Producto;

@ManagedBean
@SessionScoped
public class ProductoController implements Serializable{
    
    Conexion conn = new Conexion();
    ProductoDao productoDao;
    Producto producto;
    List<Producto> lista;

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public List<Producto> getLista() {
        return lista;
    }

    public void setLista(List<Producto> lista) {
        this.lista = lista;
    }    
    
    @PostConstruct
    public void init(){
        productoDao = new ProductoDao(conn);
        producto = new Producto();
    consultar();
    }
    
    public void consultar(){
        try {
            lista=productoDao.consultar();
        } catch (Exception e) {
        }
    }
}
