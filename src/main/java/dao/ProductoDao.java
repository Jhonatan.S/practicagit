package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Categoria;
import modelo.Producto;

public class ProductoDao {
    
    Conexion conn;

    public ProductoDao(Conexion conn) {
        this.conn = conn;
    }
    
    public List<Producto> consultar(){
    
        String sql = "select p.id_producto, p.nombre, p.precio, p.stock,c.nombre from producto as p inner join categoria as c on p.id_categoria = c.id_categoria;";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Producto> lista = new LinkedList<>();
            Producto producto;
            while (rs.next()) {                
                producto = new Producto();
                Categoria cat = new Categoria();
                producto.setId_producto(rs.getInt("id_producto"));
                producto.setNombre(rs.getString("nombre"));
                producto.setPrecio(rs.getDouble("precio"));
                producto.setStock(rs.getInt("stock"));
                cat.setNombre(rs.getString("c.nombre"));
                producto.setId_categoria(cat);
                lista.add(producto);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
