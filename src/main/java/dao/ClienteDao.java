package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Cliente;


public class ClienteDao {
    
    Conexion conn;

    public ClienteDao(Conexion conn) {
        this.conn = conn;
    }
    
    public List<Cliente> consultar(){
    
        String sql = "select * from cliente";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Cliente> lista = new LinkedList<>();
            Cliente cliente;
            while (rs.next()) {                
                cliente = new Cliente();
                cliente.setId_cliente(rs.getInt("id_cliente"));
                cliente.setNombre(rs.getString("nombre"));
                cliente.setApellido(rs.getString("apellido"));
                cliente.setDireccion(rs.getString("direccion"));
                cliente.setFecha_nacimiento(rs.getDate("fecha_nacimiento"));
                cliente.setTelefono(rs.getString("telefono"));
                cliente.setEmail(rs.getString("email"));
                lista.add(cliente);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
